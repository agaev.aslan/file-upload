const fs = require('fs')

module.exports = (io) => {
    io.on('connection', (socket) => {
        const socketId = socket.id;
 
        console.log('New client connected, socketId: ', socketId)

    var files = {}, 
    struct = { 
        name: null, 
        type: null, 
        size: 0, 
        data: [], 
        slice: 0, 
    };

    socket.on('slice upload', (data) => { 
        console.log('slice upload')
        if (!files[data.name]) { 
            files[data.name] = Object.assign({}, struct, data); 
            files[data.name].data = []; 
        }
        
        //convert the ArrayBuffer to Buffer 
        data.data = new Buffer(new Uint8Array(data.data)); 
        //save the data 
        files[data.name].data.push(data.data); 
        files[data.name].slice++;
        
        if (files[data.name].slice * 100000 >= files[data.name].size) { 
            var fileBuffer = Buffer.concat(files[data.name].data); 
            console.log('end')
            fs.writeFile(`${data.name}`, fileBuffer, (err) => { 
                // delete files[data.name]; 
                if (err) return socket.emit('upload error'); 
                if (err) console.log('upload error'); 
                socket.emit('end upload');
            });
        } else { 
            socket.emit('request slice upload', { 
                currentSlice: files[data.name].slice 
            }); 
        } 

    });

    });
};
