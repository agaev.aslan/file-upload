const express = require('express');
const socketIO = require('socket.io');
const http = require('http')
// var cors = require('cors');

const app = express();
const port = 8000;
app.set('port', port);
const corsConfig = {
  origin: true,
  credentials: true,
};
// app.use(cors(corsConfig));
// app.options('*', cors(corsConfig));

let server;
server = http.createServer(app)
.listen(port, () => {
  console.log(`App has been started on port ${port} using http`);
}); 

const io = socketIO(server);
require('./socket')(io);