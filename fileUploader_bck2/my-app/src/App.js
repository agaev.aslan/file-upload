import React, { Component } from 'react';
import io from 'socket.io-client'

// import { Client } from 'ssh2-sftp-client';
import './App.css';

const socket = io(`http://localhost:8000/`, {
  rejectUnauthorized: false,
})


class App extends Component {
  constructor(props) {
    super(props);
    this.state ={
      file:null 
    }
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
    this.fileUpload = this.fileUpload.bind(this)
  }
  

  
  onFormSubmit(e){
    e.preventDefault() // Stop form submit
    this.fileUpload(this.state.file)
  }

  onChange(e) {
    this.setState({file:e.target.files[0]})
  }

  fileUpload(file){
    const config = {
      host: 'localhost',
      username: 'aslan_ftp',
      password: 'aslan'
    };

    if(file.type.search(/^audio\//) === -1) {
          alert('Пожалуйста, загрузите аудио-файл.');
          return
    }
    
    socket.connect()

    var fileReader = new FileReader(), 
    slice = file.slice(0, 100000); 

    fileReader.readAsArrayBuffer(slice); 
    fileReader.onload = (evt) => {
        var arrayBuffer = fileReader.result; 
        socket.emit('slice upload', { 
            name: file.name, 
            type: file.type, 
            size: file.size, 
            data: arrayBuffer 
        }); 
    }

    socket.on('request slice upload', (data) => { 
      var place = data.currentSlice * 100000, 
          slice = file.slice(place, place + Math.min(100000, file.size - place)); 
      
      fileReader.readAsArrayBuffer(slice); 
    });
    
    socket.on('end upload', () => {
      console.log('File uploaded');
      alert('Загрузка завершена. Мы пришлем результаты на вашу почту через несколько минут.')
    })

    console.log('file', file)
  } 

  render() {

    
    return (
      <div className="App">
       <br/>
       <br/>
       <br/>
       <br/>
       Загрузите аудио:
       <form onSubmit={this.onFormSubmit}>
          {/* <div> Select file </div> */}
          <input type="file" onChange={this.onChange} />
          <div>
            <button type="submit">Отправить</button>
          </div>
      </form>
      </div>
    );
  }
  
}

export default App;
